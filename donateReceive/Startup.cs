﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(donateReceive.Startup))]
namespace donateReceive
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
