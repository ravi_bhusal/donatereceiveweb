﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Device.Location;

namespace donateReceive.Models
{
    public class IndexViewModel : LayoutMessageNumberViewModel
    {
        public string imgSrc { get; set; }

        public string userName { get; set; }

        public string PhoneNumber { get; set; }

        public int bloodGroupId { get; set; }

        public string BloodGroup { get; set; }

        public bool isVerified { get; set; }
    }

    public class ManageLoginsViewModel
    {
        public IList<UserLoginInfo> CurrentLogins { get; set; }
        public IList<AuthenticationDescription> OtherLogins { get; set; }
    }

    public class FactorViewModel
    {
        public string Purpose { get; set; }
    }

    public class SetPasswordViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ChangePasswordViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class AddPhoneNumberViewModel : LayoutMessageNumberViewModel
    {
        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string Number { get; set; }

        [Required]
        [Display(Name = "Blood Group")]
        [Key]
        public int bloodGroupId { get; set; }
        public IEnumerable<BloodGroup> BloodGroups { get; set; }



    }

    public class VerifyPhoneNumberViewModel:LayoutMessageNumberViewModel
    {
        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        public int pageId { get; set; }
    }

    public class ConfigureTwoFactorViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
    }

    public class PostAvailabilityViewModel
    {



        public string Id { get; set; }

        public bool available { get; set; }

        public bool inNeed { get; set; }

        public int StatusId { get; set; }

        public string userName { get; set; }

        public byte[] photo { get; set; }

        public string extraInfo { get; set; }

        public DateTime statusDateTime { get; set; }

        public string postedBloodGroup { get; set; }
        public int postedBloodGroupId { get; set; }

        public string phoneNumber { get; set; }

        public bool displayPhoneNumber { get; set; }

        public double latitude { get; set; }
        public double longitude { get; set; }

        public GeoCoordinate statusCooridnates { get; set; }


        public GeoCoordinate statusPosition { get; set; }
        public GeoCoordinate currentPosition { get; set; }

        public double distance { get; set; }

        public string facebookId { get; set; }

        public bool phoneNumberVerified { get; set; }
    }

    public class StatusViewModel : LayoutMessageNumberViewModel
    {
        [Key]
        public int? bloodGroupId { get; set; }
        public IEnumerable<BloodGroup> bloodGroups { get; set; }
        [Key]
        public int? selectBloodGroupId { get; set; }
        public IEnumerable<BloodGroup> _bloodGroups { get; set; }




        public string ButtonValue { get; set; }

        public string StatusValue { get; set; }

        public bool displayPhoneNumber { get; set; }

        public double latitude { get; set; }
        public double longitude { get; set; }

        public GeoCoordinate currentCoordinates { get; set; }

        public string radius { get; set; }


    }



    public class PostOrGetViewModel
    {


        public StatusViewModel status { get; set; }
        public IEnumerable<PostAvailabilityViewModel> post { get; set; }
    }

    public class PostCommentViewModel:LayoutMessageNumberViewModel
    {
        public string posterId { get; set; }
        public string posterName { get; set; }
        public string posterStatus { get; set; }
        public DateTime posterDate { get; set; }
        public string posterBlood { get; set; }
        public string imgSrc { get; set; }
        public string verb { get; set; }
        public bool displayNumber { get; set; }
        public string number { get; set; }
        public string distance { get; set; }
    }

    public class ChatViewModel : LayoutMessageNumberViewModel
    {

        public string chatterId { get; set; }
        public string chatterName { get; set; }
        public byte[] photo { get; set; }

    }
    public class MessagesViewModel
    {
        public string senderId { get; set; }
        public string receiverId { get; set; }
        public string userName { get; set; }
        public string messages { get; set; }

        public DateTime sentDate { get; set; }
    }

    public class liveChat
    {
        public IEnumerable<MessagesViewModel> messagesViewModel { get; set; }

        public ChatViewModel chatViewModel { get; set; }
    }

    public class MessageBoxViewModel : LayoutMessageNumberViewModel
    {
        public int id { get; set; }

        public string authorId { get; set; }


        public string receiverId { get; set; }

        public string content { get; set; }

        public DateTime sentDate { get; set; }

        public bool read { get; set; }


    }

    public class LayoutMessageNumberViewModel
    {
        public int numUnreadMessages { get; set; }

    }

    public class MessageBox
    {
        public IEnumerable<MessageBoxViewModel> messageBoxViewModel { get; set; }
    }

    public class AdminUserListViewModel:LayoutMessageNumberViewModel
    {
        public string id { get; set; }
        public string Name { get; set; }
        public byte[] imageSrc { get; set; }

        
        public int bloodGroupId { get; set; }
        public string bloodGroups { get; set; }

        public string phoneNumber { get; set; }
        public bool isVerified { get; set; }
    }

    public class AdminStatusListViewModel
    {
        public string id { get; set; }
        public string Name { get; set; }
        public byte[] imageSrc { get; set; }
        public bool isAvailable { get; set; }
        public bool isInNeed { get; set; }
        public string info { get; set; }

        public int postedBloodGroupId { get; set; }
        public string postedBloodGroup { get; set; }
        public DateTime statusDateTime { get; set; }
    }
}