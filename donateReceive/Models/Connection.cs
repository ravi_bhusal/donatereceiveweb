﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace donateReceive.Models
{
    public class Connection
    {
        [Key]
        public string ConnectionID { get; set; }

        public ApplicationUser userAgent { get; set; }
        public string userAgentId { get; set; }

        //public virtual ApplicationUser chatter { get; set; }
        //public string chatterId { get; set; }

        public bool Connected { get; set; }

    }
}