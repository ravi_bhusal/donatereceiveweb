﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace donateReceive.Models
{
    public class Message
    {
        public int Id { get; set; }

        public ApplicationUser author { get; set; }
        public string authorId { get; set; }

        public ApplicationUser receiver { get; set; }
        public string receiverId { get; set; }

        public string content { get; set; }

        public DateTime sentDate { get; set; }

        public bool read { get; set; }
    }
}