﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace donateReceive.Models
{
    public class BloodGroup
    {
        public int id { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
    }
}