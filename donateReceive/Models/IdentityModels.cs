﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace donateReceive.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
        public virtual BloodGroup BloodGroup { get; set; }

        public int bloodGroupID { get; set; }

        public bool firstLogin { get; set; }

        public bool available { get; set; }

        public bool inNeed { get; set; }

        public string extraInfo { get; set; }

        public byte[] photo { get; set; }

        public bool displayPhoneNumber { get; set; }
        public int postedBloodGroupId { get; set; }
        public DateTime statusPostTime { get; set; }



        public double latitude { get; set; }
        public double longitude { get; set; }

        public bool notification { get; set; }

       
        public ICollection<Connection> Connections { get; set; }

        public string facebookId { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<BloodGroup> bloodGroup { get; set; }
        public DbSet<Connection> Connections { get; set; }
        public DbSet<IdentityUserRole> UserRoles { get; set; }
        public virtual DbSet<Message> message { get; set; }

        public ApplicationDbContext()
            : base("Donate-Receive", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

    }
}