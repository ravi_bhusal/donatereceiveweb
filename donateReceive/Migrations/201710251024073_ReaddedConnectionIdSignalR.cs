namespace donateReceive.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReaddedConnectionIdSignalR : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "connectionId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "connectionId");
        }
    }
}
