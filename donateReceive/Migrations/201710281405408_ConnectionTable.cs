namespace donateReceive.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ConnectionTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Connections",
                c => new
                    {
                        ConnectionID = c.String(nullable: false, maxLength: 128),
                        userAgentId = c.String(maxLength: 128),
                        Connected = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ConnectionID)
                .ForeignKey("dbo.AspNetUsers", t => t.userAgentId)
                .Index(t => t.userAgentId);
            
            DropColumn("dbo.AspNetUsers", "connectionId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "connectionId", c => c.String());
            DropForeignKey("dbo.Connections", "userAgentId", "dbo.AspNetUsers");
            DropIndex("dbo.Connections", new[] { "userAgentId" });
            DropTable("dbo.Connections");
        }
    }
}
