namespace donateReceive.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BloodGroupMessageTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BloodGroups",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        authorId = c.String(maxLength: 128),
                        receiverId = c.String(maxLength: 128),
                        content = c.String(),
                        sentDate = c.DateTime(nullable: false),
                        read = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.authorId)
                .ForeignKey("dbo.AspNetUsers", t => t.receiverId)
                .Index(t => t.authorId)
                .Index(t => t.receiverId);
            
            AddColumn("dbo.AspNetUsers", "bloodGroupID", c => c.Int(nullable: false));
            CreateIndex("dbo.AspNetUsers", "bloodGroupID");
            AddForeignKey("dbo.AspNetUsers", "bloodGroupID", "dbo.BloodGroups", "id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Messages", "receiverId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Messages", "authorId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "bloodGroupID", "dbo.BloodGroups");
            DropIndex("dbo.AspNetUsers", new[] { "bloodGroupID" });
            DropIndex("dbo.Messages", new[] { "receiverId" });
            DropIndex("dbo.Messages", new[] { "authorId" });
            DropColumn("dbo.AspNetUsers", "bloodGroupID");
            DropTable("dbo.Messages");
            DropTable("dbo.BloodGroups");
        }
    }
}
