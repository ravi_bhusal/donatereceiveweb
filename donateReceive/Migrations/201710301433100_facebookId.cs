namespace donateReceive.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class facebookId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "facebookId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "facebookId");
        }
    }
}
