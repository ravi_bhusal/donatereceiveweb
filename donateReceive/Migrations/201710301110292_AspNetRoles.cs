namespace donateReceive.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AspNetRoles : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO AspNetRoles (Id, Name) Values(1, 'Admin')");
            Sql("INSERT INTO AspNetRoles (Id, Name) Values(2, 'Moderator')");
            Sql("INSERT INTO AspNetRoles (Id, Name) Values(3, 'User')");
          
        }
        
        public override void Down()
        {
        }
    }
}
