namespace donateReceive.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ConnectionIdRemoved : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AspNetUsers", "ConnectionId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "ConnectionId", c => c.String());
        }
    }
}
