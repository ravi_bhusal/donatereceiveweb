﻿namespace donateReceive.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class bloodGroups : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO BloodGroups (Id, Name) Values(1, 'AB+')");
            Sql("INSERT INTO BloodGroups (Id, Name) Values(2, 'AB−')");
            Sql("INSERT INTO BloodGroups (Id, Name) Values(3, 'B+')");
            Sql("INSERT INTO BloodGroups (Id, Name) Values(4, 'B-')");
            Sql("INSERT INTO BloodGroups (Id, Name) Values(5, 'A+')");
            Sql("INSERT INTO BloodGroups (Id, Name) Values(6, 'A-')");
            Sql("INSERT INTO BloodGroups (Id, Name) Values(7, 'O+')");
            Sql("INSERT INTO BloodGroups (Id, Name) Values(8, 'O-')");
        }
        
        public override void Down()
        {
        }
    }
}
