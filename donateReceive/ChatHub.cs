﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.AspNet.Identity;
using donateReceive.Models;
using System.Data.Entity;
using System.Threading.Tasks;

namespace donateReceive
{
    [HubName("chatHub")]
    public class ChatHub : Hub
    {


        public void Send_PrivateMessage(String senderId, String message, String _receiverId)
        {
            Clients.Caller.receiveMessage(senderId, message, _receiverId);
            string chatterId = _receiverId;
            var id = Context.User.Identity.GetUserId();

            using (var db = new ApplicationDbContext())
            {
                if (!String.IsNullOrEmpty(message))
                {

                    Message messages = new Message
                    {
                        authorId = senderId,
                        receiverId = _receiverId,
                        content = message,
                        sentDate = DateTime.Now,
                        read = false,

                    };
                    db.Entry(messages).State = EntityState.Added;
                    db.SaveChanges();

                    var receiver = db.Users.Find(_receiverId);
                    var sender = db.Users.Find(senderId);
                    if (receiver == null)
                    {
                        Clients.Caller.showErrorMessage("Could not find that user.");
                    }
                    else
                    {
                        db.Entry(receiver)
                            .Collection(u => u.Connections)
                            .Query()
                            .Where(c => c.Connected == true)
                            .Load();

                        if (receiver.Connections == null)
                        {
                            
                        }
                        else
                        {
                            foreach (var connection in receiver.Connections)
                            {


                                Clients.Client(connection.ConnectionID).receiveMessage(senderId, message, _receiverId);

                                
                            }
                        }
                    }


                }
            }
        }

        public override Task OnConnected()
        {
            
            var id = Context.User.Identity.GetUserId();
            using (var db = new ApplicationDbContext())
            {
                var user = db.Users
                    .Include(u => u.Connections)
                    .SingleOrDefault(u => u.Id == id);

                if (user == null)
                {
                    user = new ApplicationUser
                    {
                        Id = id,
                        Connections = new List<Connection>()
                    };
                    db.Users.Add(user);
                }

                user.Connections.Add(new Connection
                {
                    ConnectionID = Context.ConnectionId,
                    userAgentId = Context.Request.Headers["User-Agent"],
                    Connected = true,
                    

                });
                db.SaveChanges();
            }

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            using (var db = new ApplicationDbContext())
            {
                var connection = db.Connections.Find(Context.ConnectionId);
                connection.Connected = false;
                db.SaveChanges();
            }
            return base.OnDisconnected(stopCalled);
        }


    }
}