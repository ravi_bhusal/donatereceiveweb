﻿using donateReceive.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Device.Location;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace donateReceive.Controllers
{
    
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public bool isAvailable = false;
        public bool isInNeed = false;



        const int recordsPerPage = 8;

        public string status;
        ManageController mgc = new ManageController();

        
        public ActionResult Index()
        {
            

            string id = User.Identity.GetUserId();
            // Get the userprofile
            ApplicationUser user = db.Users.FirstOrDefault(e => e.Id == id);
            if (user != null)
            {
                if (user.available == true)
                {
                    ViewData["Status"] = "Available";
                }
                else if (user.inNeed == true)
                {
                    ViewData["Status"] = "In Need";
                }
                else
                {
                    ViewData["Status"] = "None";
                   
                }


            }
           
            string sql = String.Format("SELECT m1.* "
                                + " FROM dbo.messages m1"
                                + "  INNER JOIN"
                                + "  (SELECT t.v1 AS authorId, t.v2 AS receiverId, MAX(t.sentDate) AS maxTime "
                                + "   FROM "
                                + "   ( SELECT CASE WHEN authorId < receiverId THEN authorId ELSE receiverId END AS v1, "
                                + "      CASE WHEN authorId < receiverId THEN receiverId ELSE authorId END AS v2, "
                                + "       sentDate "
                                + "        FROM dbo.messages) t "
                                + "  GROUP BY t.v1, t.v2 ) m2 "
                                + "  ON((m1.authorId = m2.authorId AND m1.receiverId = m2.receiverId) OR(m1.authorId = m2.receiverId AND m1.receiverId = m2.authorId)) "
                                + "  AND m1.sentDate = m2.maxTime");


            var ret = db.Database.SqlQuery<Message>(sql).Where(e => e.receiverId == User.Identity.GetUserId() && e.read == false).Count();



            StatusViewModel model = new StatusViewModel()
            {
                bloodGroups = db.bloodGroup.ToList(),
                numUnreadMessages = ret,
            };



            return View(model);
        }
        [HttpPost]
        public ActionResult IndexPost(StatusViewModel viewModel)
        {
            string id = User.Identity.GetUserId();
            // Get the userprofile

            ApplicationUser user = db.Users.FirstOrDefault(e => e.Id == id);



            if (viewModel.selectBloodGroupId == null)
            {
                viewModel.selectBloodGroupId = user.bloodGroupID;
            }
            if (viewModel.displayPhoneNumber == true)
            {
                user.displayPhoneNumber = true;

            }
            else if (!viewModel.displayPhoneNumber == true)
            {
                user.displayPhoneNumber = false;
            }
            switch (viewModel.ButtonValue)
            {
                case "available":
                    isAvailable = true;
                    isInNeed = false;
                    user.postedBloodGroupId = user.bloodGroupID;
                    break;

                case "inNeed":
                    isAvailable = false;
                    isInNeed = true;
                    user.postedBloodGroupId = Convert.ToInt32(viewModel.selectBloodGroupId);
                    break;
                case "":
                    isAvailable = false;
                    isInNeed = false;
                    user.postedBloodGroupId = user.bloodGroupID;
                    break;
                default:
                    isAvailable = false;
                    isInNeed = false;
                    user.postedBloodGroupId = user.bloodGroupID;
                    break;

            }

            user.available = isAvailable;
            user.inNeed = isInNeed;
            user.extraInfo = viewModel.StatusValue;
            user.latitude = viewModel.latitude;
            user.longitude = viewModel.longitude;
            user.statusPostTime = DateTime.Now;


            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();


            //status.available = user.available;
            //status.inNeed = user.inNeed;
            //status.extraInfo = user.extraInfo;
            //status.statusPostTime = user.statusPostTime;
            //status.postedBloodGroupId = user.postedBloodGroupId;

            //db.Entry(status).State = EntityState.Modified;
            //db.SaveChanges();


            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult GetPartialView(string id, int? listValue, double latitude, double longitude, string radius, int page)
        {
            //GeoCoordinate statusPosition;

            GeoCoordinate currentPosition = new GeoCoordinate(latitude, longitude);

            if (double.IsNaN(latitude) || double.IsInfinity(latitude))
            {
                latitude = 0;
            }
            if (double.IsNaN(longitude) || double.IsInfinity(longitude))
            {
                longitude = 0;
            }
            var skipRecords = page * recordsPerPage;

            IEnumerable<PostAvailabilityViewModel> listOfPosts = (from e in db.Users
                                                                      // join j in db.status on e.Id equals j.userId
                                                                  join t in db.bloodGroup on e.postedBloodGroupId equals t.id
                                                                  select new PostAvailabilityViewModel
                                                                  {
                                                                      Id = e.Id,
                                                                      userName = e.UserName,
                                                                      extraInfo = e.extraInfo,
                                                                      photo = e.photo,
                                                                      available = e.available,
                                                                      inNeed = e.inNeed,
                                                                      postedBloodGroupId = e.postedBloodGroupId,
                                                                      postedBloodGroup = t.Name,
                                                                      phoneNumber = e.PhoneNumber,
                                                                      displayPhoneNumber = e.displayPhoneNumber,
                                                                      latitude = e.latitude,
                                                                      longitude = e.longitude,
                                                                      statusDateTime = e.statusPostTime,
                                                                      facebookId=e.facebookId,
                                                                      phoneNumberVerified=e.PhoneNumberConfirmed,


                                                                  }).Where(z => z.available == true || z.inNeed == true).OrderByDescending(j => j.statusDateTime);





            StatusViewModel _status = new StatusViewModel
            {
                latitude = latitude,
                longitude = longitude,
                radius = radius,
                bloodGroupId = listValue
            };

            var model = new PostOrGetViewModel();
            if (listValue == null)
            {
                if (id == "tab1")
                {
                    if (radius == "Global")
                    {
                        model = new PostOrGetViewModel
                        {

                            post = listOfPosts.Where(e => e.available == true).Skip(skipRecords).Take(recordsPerPage),

                            status = _status,
                        };
                    }
                    else if (radius == "0-5km")
                    {
                        model = new PostOrGetViewModel
                        {

                            post = listOfPosts.Where(e => e.available == true && currentPosition.GetDistanceTo(new GeoCoordinate(e.latitude, e.longitude)) <= 5000).Skip(skipRecords).Take(recordsPerPage),

                            status = _status,
                        };
                    }
                    else if (radius == "0-10km")
                    {
                        model = new PostOrGetViewModel
                        {

                            post = listOfPosts.Where(e => e.available == true && currentPosition.GetDistanceTo(new GeoCoordinate(e.latitude, e.longitude)) <= 10000).Skip(skipRecords).Take(recordsPerPage),

                            status = _status,
                        };
                    }
                    else if (radius == "0-20km")
                    {
                        model = new PostOrGetViewModel
                        {

                            post = listOfPosts.Where(e => e.available == true && currentPosition.GetDistanceTo(new GeoCoordinate(e.latitude, e.longitude)) <= 20000).Skip(skipRecords).Take(recordsPerPage),

                            status = _status,
                        };
                    }
                    else if (radius == "0-30km")
                    {
                        model = new PostOrGetViewModel
                        {

                            post = listOfPosts.Where(e => e.available == true && currentPosition.GetDistanceTo(new GeoCoordinate(e.latitude, e.longitude)) <= 3000).Skip(skipRecords).Take(recordsPerPage),

                            status = _status,
                        };
                    }

                }
                else
                {
                    if (radius == "Global")
                    {
                        model = new PostOrGetViewModel
                        {

                            post = listOfPosts.Where(e => e.inNeed == true).Skip(skipRecords).Take(recordsPerPage),

                            status = _status,
                        };
                    }
                    else if (radius == "0-5km")
                    {
                        model = new PostOrGetViewModel
                        {

                            post = listOfPosts.Where(e => e.inNeed == true && currentPosition.GetDistanceTo(new GeoCoordinate(e.latitude, e.longitude)) <= 5000).Skip(skipRecords).Take(recordsPerPage),

                            status = _status,
                        };
                    }
                    else if (radius == "0-10km")
                    {
                        model = new PostOrGetViewModel
                        {

                            post = listOfPosts.Where(e => e.inNeed == true && currentPosition.GetDistanceTo(new GeoCoordinate(e.latitude, e.longitude)) <= 10000).Skip(skipRecords).Take(recordsPerPage),

                            status = _status,
                        };
                    }
                    else if (radius == "0-20km")
                    {
                        model = new PostOrGetViewModel
                        {

                            post = listOfPosts.Where(e => e.inNeed == true && currentPosition.GetDistanceTo(new GeoCoordinate(e.latitude, e.longitude)) <= 20000).Skip(skipRecords).Take(recordsPerPage),

                            status = _status,
                        };
                    }
                    else if (radius == "0-30km")
                    {
                        model = new PostOrGetViewModel
                        {

                            post = listOfPosts.Where(e => e.inNeed == true && currentPosition.GetDistanceTo(new GeoCoordinate(e.latitude, e.longitude)) <= 3000).Skip(skipRecords).Take(recordsPerPage),

                            status = _status,
                        };
                    }

                }

            }
            else
            {

                if (id == "tab1")
                {
                    if (radius == "Global")
                    {
                        model = new PostOrGetViewModel
                        {

                            post = listOfPosts.Where(e => e.available == true && e.postedBloodGroupId == listValue).Skip(skipRecords).Take(recordsPerPage),

                            status = _status,
                        };
                    }
                    else if (radius == "0-5km")
                    {
                        model = new PostOrGetViewModel
                        {

                            post = listOfPosts.Where(e => e.available == true && e.postedBloodGroupId == listValue && currentPosition.GetDistanceTo(new GeoCoordinate(e.latitude, e.longitude)) <= 5000).Skip(skipRecords).Take(recordsPerPage),

                            status = _status,
                        };
                    }
                    else if (radius == "0-10km")
                    {
                        model = new PostOrGetViewModel
                        {

                            post = listOfPosts.Where(e => e.available == true && e.postedBloodGroupId == listValue && currentPosition.GetDistanceTo(new GeoCoordinate(e.latitude, e.longitude)) <= 10000).Skip(skipRecords).Take(recordsPerPage),

                            status = _status,
                        };
                    }
                    else if (radius == "0-20km")
                    {
                        model = new PostOrGetViewModel
                        {

                            post = listOfPosts.Where(e => e.available == true && e.postedBloodGroupId == listValue && currentPosition.GetDistanceTo(new GeoCoordinate(e.latitude, e.longitude)) <= 20000).Skip(skipRecords).Take(recordsPerPage),

                            status = _status,
                        };
                    }
                    else if (radius == "0-30km")
                    {
                        model = new PostOrGetViewModel
                        {

                            post = listOfPosts.Where(e => e.available == true && e.postedBloodGroupId == listValue && currentPosition.GetDistanceTo(new GeoCoordinate(e.latitude, e.longitude)) <= 3000).Skip(skipRecords).Take(recordsPerPage),

                            status = _status,
                        };
                    }

                }
                else
                {
                    if (radius == "Global")
                    {
                        model = new PostOrGetViewModel
                        {

                            post = listOfPosts.Where(e => e.inNeed == true && e.postedBloodGroupId == listValue).Skip(skipRecords).Take(recordsPerPage),

                            status = _status,
                        };
                    }
                    else if (radius == "0-5km")
                    {
                        model = new PostOrGetViewModel
                        {

                            post = listOfPosts.Where(e => e.inNeed == true && e.postedBloodGroupId == listValue && currentPosition.GetDistanceTo(new GeoCoordinate(e.latitude, e.longitude)) <= 5000).Skip(skipRecords).Take(recordsPerPage),

                            status = _status,
                        };
                    }
                    else if (radius == "0-10km")
                    {
                        model = new PostOrGetViewModel
                        {

                            post = listOfPosts.Where(e => e.inNeed == true && e.postedBloodGroupId == listValue && currentPosition.GetDistanceTo(new GeoCoordinate(e.latitude, e.longitude)) <= 10000).Skip(skipRecords).Take(recordsPerPage),

                            status = _status,
                        };
                    }
                    else if (radius == "0-20km")
                    {
                        model = new PostOrGetViewModel
                        {

                            post = listOfPosts.Where(e => e.inNeed == true && e.postedBloodGroupId == listValue && currentPosition.GetDistanceTo(new GeoCoordinate(e.latitude, e.longitude)) <= 20000).Skip(skipRecords).Take(recordsPerPage),

                            status = _status,
                        };
                    }
                    else if (radius == "0-30km")
                    {
                        model = new PostOrGetViewModel
                        {

                            post = listOfPosts.Where(e => e.inNeed == true && e.postedBloodGroupId == listValue && currentPosition.GetDistanceTo(new GeoCoordinate(e.latitude, e.longitude)) <= 30000).Skip(skipRecords).Take(recordsPerPage),

                            status = _status,
                        };
                    }

                }

            }




            switch (id)
            {
                case "tab1":

                    return PartialView("_AvailablePartial", model);

                case "tab2":
                    return PartialView("_InNeedPartial", model);

            }
            return RedirectToAction("Index");

        }




        public ActionResult MessageBoxPartial(int list)
        {

            int skipMessages = list * recordsPerPage;

            string user = User.Identity.GetUserId();


            string sql = String.Format("SELECT m1.* "
                                      + " FROM dbo.messages m1"
                                      + "  INNER JOIN"
                                      + "  (SELECT t.v1 AS authorId, t.v2 AS receiverId, MAX(t.sentDate) AS maxTime "
                                      + "   FROM "
                                      + "   ( SELECT CASE WHEN authorId < receiverId THEN authorId ELSE receiverId END AS v1, "
                                      + "      CASE WHEN authorId < receiverId THEN receiverId ELSE authorId END AS v2, "
                                      + "       sentDate "
                                      + "        FROM dbo.messages) t "
                                      + "  GROUP BY t.v1, t.v2 ) m2 "
                                      + "  ON((m1.authorId = m2.authorId AND m1.receiverId = m2.receiverId) OR(m1.authorId = m2.receiverId AND m1.receiverId = m2.authorId)) "
                                      + "  AND m1.sentDate = m2.maxTime");


            var ret = db.Database.SqlQuery<MessageBoxViewModel>(sql).OrderByDescending(f => f.sentDate).OrderByDescending(j => j.read == true).Skip(skipMessages).Take(recordsPerPage).ToList();



            return PartialView("_MessagesPartial", ret);
        }

        public ActionResult LayoutMessageCount()
        {

            return View();
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            string sql = String.Format("SELECT m1.* "
                    + " FROM dbo.messages m1"
                    + "  INNER JOIN"
                    + "  (SELECT t.v1 AS authorId, t.v2 AS receiverId, MAX(t.sentDate) AS maxTime "
                    + "   FROM "
                    + "   ( SELECT CASE WHEN authorId < receiverId THEN authorId ELSE receiverId END AS v1, "
                    + "      CASE WHEN authorId < receiverId THEN receiverId ELSE authorId END AS v2, "
                    + "       sentDate "
                    + "        FROM dbo.messages) t "
                    + "  GROUP BY t.v1, t.v2 ) m2 "
                    + "  ON((m1.authorId = m2.authorId AND m1.receiverId = m2.receiverId) OR(m1.authorId = m2.receiverId AND m1.receiverId = m2.authorId)) "
                    + "  AND m1.sentDate = m2.maxTime");


            var ret = db.Database.SqlQuery<Message>(sql).Where(e => e.receiverId == User.Identity.GetUserId() && e.read == false).Count();
            LayoutMessageNumberViewModel lmv = new LayoutMessageNumberViewModel
            {
                numUnreadMessages = ret,
            };


            return View(lmv);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            string sql = String.Format("SELECT m1.* "
                  + " FROM dbo.messages m1"
                  + "  INNER JOIN"
                  + "  (SELECT t.v1 AS authorId, t.v2 AS receiverId, MAX(t.sentDate) AS maxTime "
                  + "   FROM "
                  + "   ( SELECT CASE WHEN authorId < receiverId THEN authorId ELSE receiverId END AS v1, "
                  + "      CASE WHEN authorId < receiverId THEN receiverId ELSE authorId END AS v2, "
                  + "       sentDate "
                  + "        FROM dbo.messages) t "
                  + "  GROUP BY t.v1, t.v2 ) m2 "
                  + "  ON((m1.authorId = m2.authorId AND m1.receiverId = m2.receiverId) OR(m1.authorId = m2.receiverId AND m1.receiverId = m2.authorId)) "
                  + "  AND m1.sentDate = m2.maxTime");


            var ret = db.Database.SqlQuery<Message>(sql).Where(e => e.receiverId == User.Identity.GetUserId() && e.read == false).Count();
            LayoutMessageNumberViewModel lmv = new LayoutMessageNumberViewModel
            {
                numUnreadMessages = ret,
            };



            return View(lmv);
        }

    }
}