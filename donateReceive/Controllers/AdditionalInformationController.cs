﻿using donateReceive.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Twilio;
using static donateReceive.Controllers.ManageController;

namespace donateReceive.Controllers
{
    public class AdditionalInformationController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        ApplicationDbContext db = new ApplicationDbContext();

        public AdditionalInformationController()
        {
        }

        public AdditionalInformationController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult Add()
        {
            string sql = String.Format("SELECT m1.* "
                    + " FROM dbo.messages m1"
                    + "  INNER JOIN"
                    + "  (SELECT t.v1 AS authorId, t.v2 AS receiverId, MAX(t.sentDate) AS maxTime "
                    + "   FROM "
                    + "   ( SELECT CASE WHEN authorId < receiverId THEN authorId ELSE receiverId END AS v1, "
                    + "      CASE WHEN authorId < receiverId THEN receiverId ELSE authorId END AS v2, "
                    + "       sentDate "
                    + "        FROM dbo.messages) t "
                    + "  GROUP BY t.v1, t.v2 ) m2 "
                    + "  ON((m1.authorId = m2.authorId AND m1.receiverId = m2.receiverId) OR(m1.authorId = m2.receiverId AND m1.receiverId = m2.authorId)) "
                    + "  AND m1.sentDate = m2.maxTime");


            var ret = db.Database.SqlQuery<Models.Message>(sql).Where(e => e.receiverId == User.Identity.GetUserId() && e.read == false).Count();



            AddPhoneNumberViewModel addPhoneNumberViewModel = new AddPhoneNumberViewModel()
            {
                BloodGroups = db.bloodGroup.ToList(),
                numUnreadMessages = ret,

            };
            if (addPhoneNumberViewModel == null)
            {
                return HttpNotFound();
            }

            return View(addPhoneNumberViewModel);

        }

        // POST: AddPhoneNumberViewModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(AddPhoneNumberViewModel addPhoneNumberViewModel)
        {

            if (ModelState.IsValid)
            {

                string id = User.Identity.GetUserId();

                // Get the userprofile
                ApplicationUser user = db.Users.FirstOrDefault(e => e.Id == id);


                user.bloodGroupID = addPhoneNumberViewModel.bloodGroupId;
                user.PhoneNumber = addPhoneNumberViewModel.Number;
                db.Entry(user).State = EntityState.Modified;

                db.SaveChanges();

                //var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), addPhoneNumberViewModel.Number);
                //if (UserManager.SmsService != null)
                //{
                //    var message = new IdentityMessage
                //    {
                //        Destination = addPhoneNumberViewModel.Number,
                //        Body = "Your security code is: " + code
                //    };


                //    await UserManager.SmsService.SendAsync(message);
                //}
                return RedirectToAction("VerifyPhoneNumber", "AdditionalInformation", new { phoneNumber = addPhoneNumberViewModel.Number, pageId=1 });
            }

            return View("Index", "Home");
        }

        // GET: AddPhoneNumberViewModels/Edit/5
        [Authorize]
        public ActionResult Edit()
        {
            string id = User.Identity.GetUserId();
            ApplicationUser user = db.Users.FirstOrDefault(e => e.Id == id);
            string sql = String.Format("SELECT m1.* "
                    + " FROM dbo.messages m1"
                    + "  INNER JOIN"
                    + "  (SELECT t.v1 AS authorId, t.v2 AS receiverId, MAX(t.sentDate) AS maxTime "
                    + "   FROM "
                    + "   ( SELECT CASE WHEN authorId < receiverId THEN authorId ELSE receiverId END AS v1, "
                    + "      CASE WHEN authorId < receiverId THEN receiverId ELSE authorId END AS v2, "
                    + "       sentDate "
                    + "        FROM dbo.messages) t "
                    + "  GROUP BY t.v1, t.v2 ) m2 "
                    + "  ON((m1.authorId = m2.authorId AND m1.receiverId = m2.receiverId) OR(m1.authorId = m2.receiverId AND m1.receiverId = m2.authorId)) "
                    + "  AND m1.sentDate = m2.maxTime");


            var ret = db.Database.SqlQuery<Models.Message>(sql).Where(e => e.receiverId == User.Identity.GetUserId() && e.read == false).Count();



            AddPhoneNumberViewModel addPhoneNumberViewModel = new AddPhoneNumberViewModel()
            {
                BloodGroups = db.bloodGroup.ToList(),
                bloodGroupId = user.bloodGroupID,
                Number = user.PhoneNumber,
                numUnreadMessages = ret,

            };
            if (addPhoneNumberViewModel == null)
            {
                return HttpNotFound();
            }

            return View(addPhoneNumberViewModel);
        }

        // POST: AddPhoneNumberViewModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit(AddPhoneNumberViewModel addPhoneNumberViewModel)
        {
            if (ModelState.IsValid)
            {

                string id = User.Identity.GetUserId();
                // Get the userprofile
                ApplicationUser user = db.Users.FirstOrDefault(e => e.Id == id);
                string previousPhoneNumber = user.PhoneNumber;

                user.bloodGroupID = addPhoneNumberViewModel.bloodGroupId;
                user.PhoneNumber = addPhoneNumberViewModel.Number;
                if (addPhoneNumberViewModel.Number != previousPhoneNumber||user.PhoneNumberConfirmed==false)
                {
                    user.PhoneNumberConfirmed = false;
                }
                else
                {
                    user.PhoneNumberConfirmed = true;
                }
                db.Entry(user).State = EntityState.Modified;

                db.SaveChanges();
              
                return RedirectToAction("Index", "Manage");
               

            }

            return RedirectToAction("Index", "Manage");
        }

        public async Task<ActionResult> VerifyPhoneNumber(string phoneNumber, int pageId)
        {
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), phoneNumber);
            if (UserManager.SmsService != null)
            {
              
                var message = new IdentityMessage
                {
                    Destination = phoneNumber,
                    Body = "Your security code is: " + code
                };

                await UserManager.SmsService.SendAsync(message);
            }
            string sql = String.Format("SELECT m1.* "
                   + " FROM dbo.messages m1"
                   + "  INNER JOIN"
                   + "  (SELECT t.v1 AS authorId, t.v2 AS receiverId, MAX(t.sentDate) AS maxTime "
                   + "   FROM "
                   + "   ( SELECT CASE WHEN authorId < receiverId THEN authorId ELSE receiverId END AS v1, "
                   + "      CASE WHEN authorId < receiverId THEN receiverId ELSE authorId END AS v2, "
                   + "       sentDate "
                   + "        FROM dbo.messages) t "
                   + "  GROUP BY t.v1, t.v2 ) m2 "
                   + "  ON((m1.authorId = m2.authorId AND m1.receiverId = m2.receiverId) OR(m1.authorId = m2.receiverId AND m1.receiverId = m2.authorId)) "
                   + "  AND m1.sentDate = m2.maxTime");


            var ret = db.Database.SqlQuery<Models.Message>(sql).Where(e => e.receiverId == User.Identity.GetUserId() && e.read == false).Count();
            //var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), phoneNumber);

            var model = new VerifyPhoneNumberViewModel
            {
                numUnreadMessages = ret,
                PhoneNumber=phoneNumber,
                pageId=pageId,
            };
            // Send an SMS through the SMS provider to verify the phone number
            return /*phoneNumber == null ? View("Error") : */View(model);
        }

        //
        // POST: /Manage/VerifyPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyPhoneNumber(VerifyPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePhoneNumberAsync(User.Identity.GetUserId(), model.PhoneNumber, model.Code);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                user.PhoneNumberConfirmed = true;
                db.SaveChanges();
               
                return RedirectToAction("About", "Home");
               
                  
                
            }
            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "Failed to verify phone");
            return View(model);
        }

        public async Task<ActionResult> VerifyEditNumber(string phoneNumber)
        {
            if (phoneNumber != "")
            {
                var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), phoneNumber);
                if (UserManager.SmsService != null)
                {
                    var message = new IdentityMessage
                    {
                        Destination = phoneNumber,
                        Body = "Your security code is: " + code
                    };


                    await UserManager.SmsService.SendAsync(message);
                }
                return RedirectToAction("VerifyPhoneNumber", "AdditionalInformation", new { phoneNumber = phoneNumber });
            }
            return View();
        }

    }
}