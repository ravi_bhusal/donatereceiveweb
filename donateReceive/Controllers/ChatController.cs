﻿using donateReceive.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace donateReceive.Controllers
{
    [System.Web.Mvc.Authorize]
    public class ChatController : Controller
    {
        public static string _receiverId;
        string id;

        int messagesPerPage = 10;
        int skipMessages;
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: Chat
        public ActionResult Index()
        {

           
            return View();
        }
        [System.Web.Mvc.Authorize]
        public ActionResult Chat(string _Id)
        {
            if (_Id != null)
            {
                _receiverId = _Id;
                ApplicationUser user = db.Users.FirstOrDefault(e => e.Id == _Id);

               
                string sql = String.Format("SELECT m1.* "
               + " FROM dbo.messages m1"
               + "  INNER JOIN"
               + "  (SELECT t.v1 AS authorId, t.v2 AS receiverId, MAX(t.sentDate) AS maxTime "
               + "   FROM "
               + "   ( SELECT CASE WHEN authorId < receiverId THEN authorId ELSE receiverId END AS v1, "
               + "      CASE WHEN authorId < receiverId THEN receiverId ELSE authorId END AS v2, "
               + "       sentDate "
               + "        FROM dbo.messages) t "
               + "  GROUP BY t.v1, t.v2 ) m2 "
               + "  ON((m1.authorId = m2.authorId AND m1.receiverId = m2.receiverId) OR(m1.authorId = m2.receiverId AND m1.receiverId = m2.authorId)) "
               + "  AND m1.sentDate = m2.maxTime");


                var ret = db.Database.SqlQuery<Message>(sql).Where(e => e.receiverId == User.Identity.GetUserId() && e.read == false).Count();
               
                ChatViewModel model = new ChatViewModel
                {
                    chatterId = _Id,
                    chatterName = user.UserName,
                    photo = user.photo,
                    numUnreadMessages = ret,
                };



                return View(model);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

        }
        [HttpPost]
        public void PostMessage(string content)
        {

            id = User.Identity.GetUserId();
            if (content != "")
            {

                Message messages = new Message
                {
                    authorId = id,
                    receiverId = _receiverId,
                    content = content,
                    sentDate = DateTime.Now,
                    read = false,

                };
                db.Entry(messages).State = EntityState.Added;
                db.SaveChanges();








            }
        }

        public ActionResult ChatPartialView(int page)
        {

            skipMessages = page * messagesPerPage;
            string id = User.Identity.GetUserId();

            ApplicationUser user = db.Users.FirstOrDefault(e => e.Id == _receiverId);
            IEnumerable<MessagesViewModel> mess = (from e in db.message

                                                   select new MessagesViewModel
                                                   {
                                                       senderId = e.authorId,
                                                       receiverId = e.receiverId,
                                                       messages = e.content,
                                                       sentDate = e.sentDate,



                                                   }).Where(e => e.senderId == _receiverId && e.receiverId == id || (e.senderId == id && e.receiverId == _receiverId));

            ChatViewModel chat = new ChatViewModel
            {
                chatterId = _receiverId,
                photo = user.photo,


            };

            liveChat model = new liveChat
            {
                messagesViewModel = mess.OrderByDescending(i => i.sentDate).Skip(skipMessages).Take(messagesPerPage),
                chatViewModel = chat,
            };

            var message = db.message.Where(e => e.authorId == _receiverId && e.receiverId == id).OrderByDescending(i => i.sentDate).FirstOrDefault();
            if (message != null)
            {
                if (message.read == false)
                {
                    message.read = true;
                    db.Entry(message).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }







            return PartialView("_ChatPartial", model);
        }
    }
}