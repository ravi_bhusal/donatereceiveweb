﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace donateReceive.Controllers
{
    public class ErrorController : Controller
    {
        public ViewResult Index()
        {
            return View("Error");
        }
        public ViewResult Error(string aspxerrorpath)
        {
           
            Response.StatusCode = 404;  //you may want to set this to 200
            return View("Error");
        }
    }
}