﻿using donateReceive.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace donateReceive.Controllers
{
    [Authorize]
    public class PostCommentController : Controller
    {
        // GET: PostComment

        ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {

            return View();
        }
        [Authorize]
        public ActionResult Comments(string _Id, string _postedBlood, string verb, string _distance)
        {
            if (_Id != null)
            {
                ApplicationUser user = db.Users.FirstOrDefault(e => e.Id == _Id);
                var base64 = Convert.ToBase64String(user.photo);
                var imgSrc = String.Format("data:image/gif;base64,{0}", base64);
                PostCommentViewModel model = new PostCommentViewModel
                {
                    posterId = _Id,
                    posterName = user.UserName,
                    posterStatus = user.extraInfo,
                    posterBlood = _postedBlood,
                    posterDate = user.statusPostTime,
                    imgSrc = imgSrc,
                    verb = verb,
                    displayNumber = user.displayPhoneNumber,
                    number = user.PhoneNumber,
                    distance = _distance,



                };

                return View(model);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public void PostNotification(string id)
        {

            ApplicationUser user = db.Users.FirstOrDefault(e => e.Id == id);
            user.notification = true;
            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();

        }
    }
}