﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using donateReceive.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace donateReceive.Controllers
{
    public class AdminController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        int itemsPerPage = 10;
        public ActionResult Dashboard()
        {
            string Id = User.Identity.GetUserId();
            IdentityUserRole userRole = db.UserRoles.FirstOrDefault(e => e.UserId == Id);
            if (userRole.RoleId == "1")
            {
                string sql = String.Format("SELECT m1.* "
                   + " FROM dbo.messages m1"
                   + "  INNER JOIN"
                   + "  (SELECT t.v1 AS authorId, t.v2 AS receiverId, MAX(t.sentDate) AS maxTime "
                   + "   FROM "
                   + "   ( SELECT CASE WHEN authorId < receiverId THEN authorId ELSE receiverId END AS v1, "
                   + "      CASE WHEN authorId < receiverId THEN receiverId ELSE authorId END AS v2, "
                   + "       sentDate "
                   + "        FROM dbo.messages) t "
                   + "  GROUP BY t.v1, t.v2 ) m2 "
                   + "  ON((m1.authorId = m2.authorId AND m1.receiverId = m2.receiverId) OR(m1.authorId = m2.receiverId AND m1.receiverId = m2.authorId)) "
                   + "  AND m1.sentDate = m2.maxTime");


                var ret = db.Database.SqlQuery<Message>(sql).Where(e => e.receiverId == User.Identity.GetUserId() && e.read == false).Count();
                LayoutMessageNumberViewModel lmv = new LayoutMessageNumberViewModel
                {
                    numUnreadMessages = ret,
                };


                return View(lmv);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult UserList(int page)
        {
            var skipItems = page * itemsPerPage;
            IEnumerable<AdminUserListViewModel> uvm = (from e in db.Users
                                               join j in db.bloodGroup on e.bloodGroupID equals j.id
                                               select new AdminUserListViewModel
                                               {
                                                   id = e.Id,
                                                   Name = e.UserName,
                                                   imageSrc = e.photo,
                                                   bloodGroupId = e.bloodGroupID,
                                                   bloodGroups = j.Name,
                                                   phoneNumber=e.PhoneNumber,
                                                   isVerified=e.PhoneNumberConfirmed,
                                               });



            return PartialView("_UserList",uvm.Skip(skipItems).Take(itemsPerPage));
        }

        public ActionResult StatusList(int page)
        {
            var skipItems = page * itemsPerPage;
            IEnumerable<AdminStatusListViewModel> svm = (from e in db.Users
                                                       join j in db.bloodGroup on e.bloodGroupID equals j.id
                                                       select new AdminStatusListViewModel 
                                                       {
                                                           id = e.Id,
                                                           Name = e.UserName,
                                                           imageSrc = e.photo,
                                                           isAvailable=e.available,
                                                           isInNeed=e.inNeed,
                                                           postedBloodGroupId = e.postedBloodGroupId,
                                                           postedBloodGroup = j.Name,
                                                           statusDateTime=e.statusPostTime,
                                                           info=e.extraInfo,
                                                          
                                                       }).Where(z => z.isAvailable == true || z.isInNeed == true).OrderByDescending(j => j.statusDateTime).Skip(skipItems).Take(itemsPerPage);

            return PartialView("_StatusList",svm);
        }
      
        public ActionResult DeleteUser(string id)
        {
            var userToDelete = db.Users.FirstOrDefault(e=>e.Id==id);
            var message=  db.message.Where(e => e.authorId == id || e.receiverId == id);
            var connection = db.Connections.Where(e => e.userAgentId == id);
            var userRoles = db.UserRoles.Where(e => e.UserId == id);

            db.message.RemoveRange(message);
            db.Connections.RemoveRange(connection);
            db.UserRoles.RemoveRange(userRoles);
            db.Users.Remove(userToDelete);
            db.SaveChanges();

            return RedirectToAction("Dashboard","Admin");
        }

        public ActionResult DeleteStatus(string id)
        {
            
            var statusToDelete = db.Users.FirstOrDefault(e => e.Id == id);
            statusToDelete.available = false;
            statusToDelete.inNeed = false;
            statusToDelete.extraInfo = "";
            statusToDelete.postedBloodGroupId = statusToDelete.bloodGroupID;
            db.Entry(statusToDelete).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Dashboard", "Admin");
        }
    }
}